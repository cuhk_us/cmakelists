# README #
This is repo is for the upgraded version of ddrone with vio, mapping, human tracking etc. functions.
The ten modules are as follow:
- nus_launch(branch: kinetic_Ddrone)
    - The project's launch file for both simulation and real flight test.
- mavros(branch: ddrone_v2)
    - Communication between ROS and Pixhawk
- nus_msgs(branch: kinetic_Ddrone)
   -  Legacy msgs
- nus_unity_socket(branch: junji)
    - Communication between simulator and other applications
- nus_zed_wrapper(branch: master)
    - ZED canera driver
- ekf_sensor_fusion(branch: ddrone)
    - EKF VIO
- pub_uav_state(branch: zed-odom)
    - Pushlish vio info to pixhawk. Set up in this repo if you want to provide to the localizaion result from other resources, such as GPS, UWB, VIO
- nus_human_tracking(branch: zed)
    - Human tracking
- nus_yolo_detection(branch: master)
    - Target detection
- us_ws(branch: master): Utilities from shupeng's side.
    - common_msgs
        - Self defined msgs for pixhawk controller 
    - usr_console
        - Send command through terminal. Can use rqt to subsititute this package.
    - rt_ref_gen
        -Generate reference
	- edt(branch: linear_dt)
        - mapping
	- nndp_cpp(branch: discrete_pso)
        - path planning module

*The packages not mentioned here are not used by the ddrone project.*
### Set up 
#### 1. Download 
```
Please use --recurse-submodules to download all the submodules.
$ git clone --recurse-submodules https://bitbucket.org/nusuav/ddrone_v2.git
```
And then input your user name and password. 
To avoid entering username / password each time, you can store credentials into cache with below command:
```
$ git config --global credential.helper 'cache --timeout 3600'
```
where 3600 (seconds) means 1 hour, you may change it as per your requirement.

#### 2. Compile

```
$ cd catkin_ws
$ catkin build --cmake-args -DCMAKE_BUILD_TYPE=Release
```

#### 3. Submodule config (Optional)
Do this if you want to make any changes to the code.
- a. Add [alias] in local .git/config to make each submodule track the newest commit in their branch
```
[alias]
sm-trackbranch = "! git submodule foreach --recursive 'branch=\"$(git config -f $toplevel/.gitmodules submodule.$name.branch)\"; git checkout $branch'"
```
- b. Run
```
$ git sm-trackbranch
```
- c. Add push setting in global .gitconfig file under /home/nvidia.
To make your commit of submodule trackable
```
[push]
	default = matching 
```

### Usage
#### step 1: common setting before testing
1. system_id 
Change the system_id (pixhawk id) in v2_Ddrone*.launch, usr_console.launch, px4_config.yaml(uav_sys_id_list: [system_id, 255] ) as the pixhawk's id.

2. waypoints file location
Put waypoints in ddrone_v2/param/waypoints.txt or change the file location and name in usr_console.launch (waypoints_file param) to meet your requirement. 

3. Flight parameters(optional)
- /ddrone_v2/us_ws/nndp_cpp/include/optimization/OptimizationUtilities.h
    - Safety distance: **rTooClose**(default: 1.0)
    - Vehicle size: **rVehicle**(default: 0.4)

- /ddrone_v2/us_ws/edt/src/edt_node_laser_realdrone.cpp
    - compress range for 2d mapping: (default: **FLYHEIGHT-0.4, FLYHEIGHT+0.2**)
        - su->updateEDTMap(FLYHEIGHT-0.4, FLYHEIGHT+0.2, center);
- /ddrone_v2/us_ws/nndp_cpp/src/nndp_cpp_node.cpp
    - flight speed: **ran.loc_h**(default: 2), **ran.loc_v**(default: 1)
       Modify from 1(low speed) to 5(high speed).

4. [zed camera parameters](https://www.stereolabs.com/docs/ros/zed_node/) (optional)
- Please put calibration param under /usr/local/zed/settings with SNxxxx.conf as it's name
- Change camera param in edt.yaml file under edt package.
- Change frequency of zed from tools/ZED Exlplorer
    - Remember to change frequency param in zed.launch file
    - Remember to change the noise param in vision_sensor_fix_ddrone.yaml as well 
- If you want to use zed only instead of VIO, change the use_zed_only param in v2_Ddrone.launch to true and change the the param file under edt from edt.yaml to edt_mini.yaml

5. system checking
    - reset pixhawk (hardware reset for now)
    - check vio status, whether the location and heading is correct
    - check mapping status, whether or not there are passable area to fly

6. zed auto explosure
   rosrun rqt_reconfigure rqt_reconfigure

#### step 2: platform specific command
1. simulator
```
$ roslaunch nus_launch v2_Ddrone_sim.launch
```
2. Real flight
```
$ roslaunch nus_launch v2_Ddrone.launch
```
*if you want to test pixhawk only without other high level modules, then just run "roslaunch mavros px4.launch"*
#### step 3: take off
```
$ roslaunch usr_console usr_console.launch 
```
- 1. Clear previous reference on pixhawk
wait till all the plugins have been loaded and them input **engine0** command in the terminal.
- 2. take off
Input **Engage** command in the terminal.
- 3. Send waypoints command.
    - Global waypoints: **mission*** 
    This command will load the waypoints.txt where all the points are in global NWU frame (True North).
    - Local waypoints: **waypoints*** 
    This command will load the waypoints.txt file but all the waypoints is in local NWU frame.

- 4. show path in rviz(optional)
run the uav_state_to_path.py under nus_launch/scripts folder.
```
$ python uav_state_to_path.py
```

#### step 4: landing
Ask Redhwan lah!

#### Clear pixhawk flight status
- Engine 0 (Not working and to be tested)
```
rosservice call /mavros/cmd/set_home '{current_gps: 1}'
```

### Others
#### Attention
Please remove user name in the http link when adding new modules to make it easier for other ppl to manage the repo.

#### Check GPU usage
```
$ sudo ~/tegrastats --interval <int> --logfile <out_file> &
For example:
$ sudo ~/tegrastats --interval 5000 --logfile ~/ddrone #This command probes every 5 seconds
```
The out put is similar to this:
```
RAM 2135/7855MB (lfb 1057x4MB) CPU [2%@2035,0%@2035,0%@2035,2%@2035,2%@2036,2%@2035] EMC_FREQ 1%@1866 GR3D_FREQ 0%@1300 APE 150 MTS fg 0% bg 0% BCPU@30C MCPU@30C GPU@29C PLL@30C Tboard@28C Tdiode@28.25C PMIC@100C thermal@29.4C VDD_IN 3104/4029 VDD_CPU 291/730 VDD_GPU 145/207 VDD_SOC 727/746 VDD_WIFI 0/180 VDD_DDR 1248/1375
```
Please check [tegrastats utility reports](https://docs.nvidia.com/jetson/l4t/index.html#page/Tegra%2520Linux%2520Driver%2520Package%2520Development%2520Guide%2FAppendixTegraStats.html) to get more info about the stats.
The data we normally care are:
- **RAM X/Y**: X is the amount of RAM in use in MB.
- **CPU [X%@Z, Y%@Z,...]**: Load statistics for each of the CPU cores relative to the current running frequency Z.
- **GR3D_FREQ X%@Y**: Percent of the GR3D that is being used, relative to the current running frequency. GR3D is the GPU engine.
 
#### Hotspot setup
1. Enable broadcast of SSID (self wifi mode)
```
$ echo 2 > /sys/module/bcmdhd/parameters/op_mode
```
Change back to normal wifi mode
```
$ echo 0 > /sys/module/bcmdhd/parameters/op_mode
```

2. To make it persistent
Add line `options bcmdhd op_mode=2` in `nano /etc/modprobe.d/bcmdhd.conf`

3. Change ip address
Add line `address1=192.168.1.150/24`

#### Fan
1. stop fan
```
$ sudo -s 
$ cd /sys/kernel/debug/tegra_fan/
$ echo 0 > target_pwm
```
2. Check temperature
```
$ watch -n 1 cat /sys/class/thermal/thermal_zone?/temp 
```

#### Submodule
1. Add a new submodule with a branch name
```
$ git submodule add -b branch_name  https://bitbucket.org/nusuav/repo_name.git
```
2. Remove a submodule
```
$ git submodule deinit -f submodule_name (The name of the submodule is the same as the repo's name)   
$ rm -rf .git/modules/submodule_name
$ git rm -f submodule_name
```
3. Other useful [alias] in local .git/config 
```
[alias]
#git sm-trackbranch : places all submodules on their respective branch specified in .gitmodules
#This works if submodules are configured to track a branch, i.e if .gitmodules looks like :
#[submodule "my-submodule"]
#   path = my-submodule
#   url = git@wherever.you.like/my-submodule.git
#   branch = my-branch
sm-trackbranch = "! git submodule foreach --recursive 'branch=\"$(git config -f $toplevel/.gitmodules submodule.$name.branch)\"; git checkout $branch'"

#sm-pullrebase :
# - pull --rebase on the master repo
# - sm-trackbranch on every submodule
# - pull --rebase on each submodule
#
# Important note :
#- have a clean master repo and subrepos before doing this !
#- this is *not* equivalent to getting the last committed 
#  master repo + its submodules: if some submodules are tracking branches 
#  that have evolved since the last commit in the master repo,
#  they will be using those more recent commits !
#
#  (Note : On the contrary, git submodule update will stick 
#to the last committed SHA1 in the master repo)
#
sm-pullrebase = "! git pull --rebase; git submodule update; git sm-trackbranch ; git submodule foreach 'git pull --rebase' "

# git sm-diff will diff the master repo *and* its submodules
sm-diff = "! git diff && git submodule foreach 'git diff' "

#git sm-push will ask to push also submodules
sm-push = push --recurse-submodules=on-demand

#git alias : list all aliases
alias = "!git config -l | grep alias | cut -c 7-"
```





